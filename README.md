# [JsonNormalizer](https://rui4moura.gitlab.io/jsonnormalizer/)

This library allows you to get a flattened file from a JSON with a complex structure, but not the usual flat JSON where you get everything on the same level, this will get you all the fields that are unique in the JSON and create an array of those. 
