package io.gitlab.rui4moura.jsonnormalizer;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.ValueNode;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.util.logging.Logger.getLogger;


public final class JsonUtils {
    private static final Logger log = getLogger("JsonUtils");

    private JsonUtils() {
        //hides implicit public constructor
    }

    public static void normalize(File inputFile, File outputFile, String jsonRoot, String jsonStop,
            Boolean isInclusive, String... fieldsToIgnore) {
        var mapper = new JsonMapper();
        mapper.registerModule(new JavaTimeModule());
        var first = true;

        try (var reader = new BufferedReader(new InputStreamReader(new FileInputStream(inputFile)))) {
            var jsonNodeIterator = new JsonNodeIterator(reader);
            if (jsonRoot != null && !jsonRoot.equals("") && !jsonNodeIterator.findJsonRoot(jsonRoot)) {
                //reset reader and read from the top
                normalize(inputFile, outputFile, "", jsonStop, isInclusive, fieldsToIgnore);
                return;
            }
            try (var bw = new BufferedWriter(new FileWriter(outputFile))) {
                Writer.setBw(bw);
                Writer.arrayStartBoxBracket();
                iterateThroughJsonNode(jsonStop, isInclusive, first, jsonNodeIterator, fieldsToIgnore);
                Writer.arrayEndBoxBracket();
            }
        } catch (IOException e) {
            log.log(Level.SEVERE, "Cannot read Json tempFile");
        }
    }

    private static Long normalizeNode(JsonNode node, String own, String parent, Map<String, JsonNode> currentNodeMap,
            Map<String, Integer> controlMap, TreeLevel level) throws IOException {
        Long jsonElementsCount = level == null ? 0L : level.getCount();
        //decides if still in the current node or in a new node
        if (node instanceof ValueNode) {
            jsonElementsCount = decideIfStillInCurrentNodeOrNewNode(own, currentNodeMap, controlMap,
                    jsonElementsCount, level, (ValueNode) node);
        } else {
            //
            String prefix = parent == null ? "" : parent + ".";
            var branchLevel = createTreeLevel(level, jsonElementsCount);
            jsonElementsCount = processNode(node, currentNodeMap, controlMap, jsonElementsCount, prefix,
                    branchLevel);
        }
        return jsonElementsCount;
    }


    private static Long processNode(JsonNode node, Map<String, JsonNode> currentNodeMap,
            Map<String, Integer> controlMap, Long count, String prefix, TreeLevel level)
            throws IOException {
        // Controlling the stop level
        if (Boolean.TRUE.equals(level.isStop())) {
            currentNodeMap.put(Appender.STOPNODE, node);
        }
        if (node instanceof ArrayNode) {
            count = processArrayNode((ArrayNode) node, currentNodeMap, controlMap, count, prefix, level);
        } else if (node instanceof ObjectNode) {
            count = processAllFieldsFromObjectNode((ObjectNode) node, currentNodeMap, controlMap, count, prefix,
                    level);
        } else {
            throw new IOException(new Throwable("unknown json node"));
        }
        return count;
    }

    private static Long processAllFieldsFromObjectNode(ObjectNode objectNode, Map<String, JsonNode> currentNodeMap,
            Map<String, Integer> controlMap, Long count, String prefix, TreeLevel level) throws IOException {
        for (Iterator<Map.Entry<String, JsonNode>> it = objectNode.fields(); it.hasNext(); ) {
            Map.Entry<String, JsonNode> field = it.next();
            level.setCount(count);
            if (field.getKey().equals(level.getJsonStop())) {
                level.setStop(level.getLevel() + 1);
            }
            count = normalizeNode(field.getValue(), field.getKey(), prefix + field.getKey(), currentNodeMap, controlMap,
                    level);
        }
        return count;
    }

    private static Long processArrayNode(ArrayNode arrayNode, Map<String, JsonNode> currentNodeMap,
            Map<String, Integer> controlMap, Long count, String prefix, TreeLevel level) throws IOException {
        for (var i = 0; i < arrayNode.size(); i++) {
            level.setCount(count);
            count = normalizeNode(arrayNode.get(i), null, prefix + i, currentNodeMap, controlMap, level);
        }
        return count;
    }

    private static Long decideIfStillInCurrentNodeOrNewNode(String own, Map<String, JsonNode> currentNodeMap,
            Map<String, Integer> controlMap, Long count, TreeLevel level, ValueNode node) throws IOException {
        if (level == null || level.getStop() == null || level.getStop().equals(0) ||
                (level.isInclusive() && level.getLevel() == level.getStop() + 1)) {
            count = controlMapManagement(own, currentNodeMap, controlMap, count, level, node);
        }
        return count;
    }

    private static Long controlMapManagement(String own, Map<String, JsonNode> currentNodeMap,
            Map<String, Integer> controlMap, Long count, TreeLevel level, ValueNode node) throws IOException {
        if (controlMap.containsKey(own) && level != null && !level.getFieldsToIgnore().contains(own)) {
            Integer myLevel = controlMap.get(own);
            controlMap.values().removeIf(val -> val >= myLevel);
            Appender.appendItem(currentNodeMap, count, level);
            count++;
        }
        Integer levelValue = level != null ? level.getLevel() : 0;
        controlMap.put(own, levelValue);
        currentNodeMap.put(own, node);
        return count;
    }


    private static TreeLevel createTreeLevel(TreeLevel level, Long jsonElementsCount) {
        var branchLevel = new TreeLevel(level == null ? 0 : level.getLevel(), jsonElementsCount,
                level == null ? 0 : level.getStop(),
                level == null ? "" : level.getJsonStop(),
                level == null ? Boolean.FALSE : level.isInclusive(),
                level == null ? Collections.emptySet() : level.getFieldsToIgnore());
        branchLevel.increment();
        return branchLevel;
    }

    private static void startNodeNormalizer(JsonNode input, String jsonStop, Boolean isInclusive,
            String... fieldsToIgnore) {
        Map<String, JsonNode> currentNodeMap = new LinkedHashMap<>();
        Map<String, Integer> controlMap = new HashMap<>();
        try {
            var level = new TreeLevel(0, 0L, 0, jsonStop, isInclusive, Set.of(fieldsToIgnore));
            Long count = normalizeNode(input, null, null, currentNodeMap, controlMap, level);
            Appender.appendItem(currentNodeMap, count, level);
        } catch (IOException e) {
            log.log(Level.SEVERE, "could not open temporary file for writing");
        }
    }

    private static void iterateThroughJsonNode(String jsonStop, Boolean isInclusive, boolean first,
            JsonNodeIterator jsonNodeIterator, String... fieldsToIgnore) throws IOException {
        while (jsonNodeIterator.hasNext()) {
            var node = jsonNodeIterator.next();
            first = Writer.commaIfNotFirstItem(first);
            startNodeNormalizer(node, jsonStop, isInclusive, fieldsToIgnore);
        }
    }


    public static void main(String[] args) {
        // left blank
    }

}
