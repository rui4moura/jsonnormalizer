package io.gitlab.rui4moura.jsonnormalizer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.json.JsonMapper;

import java.io.IOException;
import java.io.Reader;
import java.util.Iterator;
import java.util.NoSuchElementException;

class JsonNodeIterator implements Iterator<JsonNode> {

    private JsonMapper mapper;
    private JsonParser parser;

    public JsonNodeIterator(Reader reader) throws IOException {
        mapper = new JsonMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        parser = mapper.getFactory().createParser(reader);
        skipStart();
    }

    public JsonNodeIterator() {
        // used for mocking
    }

    public void setMapper(JsonMapper mapper) {
        this.mapper = mapper;
    }

    public void setParser(JsonParser parser) {
        this.parser = parser;
    }

    private void skipStart() throws IOException {
        while (parser.currentToken() != JsonToken.START_OBJECT) {
            if (parser.nextToken() == null) {
                break;
            }
        }
    }

    boolean findJsonRoot(String jsonRoot) throws IOException {
        var found = false;
        while (parser.nextToken() != null) {
            if (jsonRoot.equals(parser.getCurrentName())) {
                found = true;
                while (parser.currentToken() != JsonToken.START_OBJECT) {
                    if (parser.nextToken() == null) {
                        break;
                    }
                }
                break;
            }
        }
        return found;
    }

    @Override
    public boolean hasNext() {
        try {
            while (parser.currentToken() == null) {
                if (parser.nextToken() == null) {
                    break;
                }
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

        return parser.currentToken() == JsonToken.START_OBJECT;
    }

    @Override
    public JsonNode next() {
        try {
            @SuppressWarnings("java:S6212")
            JsonNode jsonNode = parser.readValueAsTree();
            if (jsonNode.isNull()) {
                throw new NoSuchElementException();
            }
            return jsonNode;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

}
