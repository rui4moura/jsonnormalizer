package io.gitlab.rui4moura.jsonnormalizer;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.ValueNode;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

public final class Appender {
    static final String STOPNODE = "STOPNODE";

    private Appender() {
        // hides the public implicit
    }

    static void appendItem(Map<String, JsonNode> currentNodeMap, Long count, TreeLevel level)
            throws IOException {
        if (count > 0) {
            Writer.comma();
        }
        Writer.startCurlyBracket();
        var countElements = 0;
        for (Map.Entry<String, JsonNode> entry : currentNodeMap.entrySet()) {
            String entryKey = entry.getKey();
            if (!entryKey.equals(STOPNODE) && level != null && !level.getFieldsToIgnore().contains(entryKey)) {
                if (countElements > 0) {
                    Writer.comma();
                }
                appendValueNodeElement(entryKey, (ValueNode) entry.getValue());
                countElements++;
            }
        }
        if (level != null && level.getJsonStop() != null && !level.isInclusive() &&
                currentNodeMap.containsKey(STOPNODE)) {
            appendJsonNodeElement(level.getJsonStop(), currentNodeMap.get(STOPNODE));
        } else if (level != null && level.isInclusive() && currentNodeMap.containsKey(STOPNODE)) {
            appendElements(currentNodeMap, count, level, currentNodeMap.get(STOPNODE));
        }
        Writer.endCurlyBracket();
    }

    private static void appendElements(Map<String, JsonNode> currentNodeMap, Long count,
            TreeLevel level, JsonNode node)
            throws IOException {
        if (level != null && level.isInclusive() && node instanceof ObjectNode) {
            appendAllFieldsFromObjectNode(currentNodeMap, (ObjectNode) node);
        } else if (level != null && level.isInclusive() && node instanceof ArrayNode) {
            appendElementsFromArrayNode(currentNodeMap, count, level, (ArrayNode) node);
        }
    }

    private static void appendElementsFromArrayNode(Map<String, JsonNode> currentNodeMap, Long count,
            TreeLevel level, ArrayNode arrayNode) throws IOException {
        for (var i = 0; i < arrayNode.size(); i++) {
            appendElements(currentNodeMap, count, level, arrayNode.get(i));
        }
    }

    private static void appendAllFieldsFromObjectNode(Map<String, JsonNode> currentNodeMap,
            ObjectNode objectNode) throws IOException {
        Boolean include = isIncludable(currentNodeMap, objectNode);
        for (Iterator<Map.Entry<String, JsonNode>> it = objectNode.fields(); it.hasNext(); ) {
            Map.Entry<String, JsonNode> field = it.next();
            appendElementIfIncludable(currentNodeMap, include, field);
        }
    }

    private static void appendElementIfIncludable(Map<String, JsonNode> currentNodeMap,
            Boolean include, Map.Entry<String, JsonNode> field) throws IOException {
        if (Boolean.TRUE.equals(include) && !currentNodeMap.containsKey(field.getKey())) {
            if (field.getValue() instanceof ValueNode) {
                Writer.comma();
                appendValueNodeElement(field.getKey(), (ValueNode) field.getValue());
            } else {
                appendJsonNodeElement(field.getKey(), field.getValue());
            }
        }
    }

    private static void appendValueNodeElement(String key, ValueNode value) throws IOException {
        Writer.jsonKey(key);
        Writer.valueNodeWithQuotesIfNeeded(value);
    }

    private static void appendJsonNodeElement(String key, JsonNode value) throws IOException {
        Writer.comma();
        Writer.jsonKey(key);
        Writer.jsonNodeWithQuotesIfNeeded(value);
    }

    private static Boolean isIncludable(Map<String, JsonNode> currentNodeMap, ObjectNode objectNode) {
        Boolean include = Boolean.FALSE;
        for (Iterator<Map.Entry<String, JsonNode>> it = objectNode.fields(); it.hasNext(); ) {
            Map.Entry<String, JsonNode> field = it.next();
            if (currentNodeMap.containsKey(field.getKey())) {
                include = matchesTheFieldValueForThisNodeItem(currentNodeMap, field);
                if (Boolean.FALSE.equals(include)) {
                    break;
                }
            }
        }
        return include;
    }

    private static boolean matchesTheFieldValueForThisNodeItem(Map<String, JsonNode> currentNodeMap,
            Map.Entry<String, JsonNode> field) {
        return currentNodeMap.get(field.getKey()).equals(field.getValue());
    }

}
