package io.gitlab.rui4moura.jsonnormalizer;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ValueNode;

import java.io.BufferedWriter;
import java.io.IOException;

public final class Writer {

    private static BufferedWriter bw;

    private Writer() {
        // hides the public constructor
    }

    public static void setBw(BufferedWriter bw) {
        Writer.bw = bw;
    }

    static boolean commaIfNotFirstItem(boolean first) throws IOException {
        if (first) {
            first = false;
        } else {
            bw.write(",");
        }
        return first;
    }

    static void arrayEndBoxBracket() throws IOException {
        bw.write("]");
    }

    static void arrayStartBoxBracket() throws IOException {
        bw.write("[");
    }

    static void valueNodeWithQuotesIfNeeded(ValueNode value) throws IOException {
        quotesIfTextual(value);
        bw.write(value.asText());
        quotesIfTextual(value);
    }

    static void jsonNodeWithQuotesIfNeeded(JsonNode value) throws IOException {
        quotesIfTextual(value);
        bw.write(value.toString());
        quotesIfTextual(value);
    }

    static void jsonKey(String key) throws IOException {
        bw.write("\"");
        bw.write(key);
        bw.write("\": ");
    }

    static void startCurlyBracket() throws IOException {
        bw.write("{");
    }

    static void endCurlyBracket() throws IOException {
        bw.write("}");
    }

    static void comma() throws IOException {
        bw.write(",");
    }

    private static void quotesIfTextual(JsonNode value) throws IOException {
        if (value.isTextual()) {
            bw.write("\"");
        }
    }
}
