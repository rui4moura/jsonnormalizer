package io.gitlab.rui4moura.jsonnormalizer;

import java.util.Set;

class TreeLevel {
    private Integer level;
    private Long count;
    private Integer stop;
    private String jsonStop;
    private Set<String> fieldsToIgnore;
    private Boolean isInclusive;

    public TreeLevel(Integer level, Long count, Integer stop, String jsonStop, Boolean isInclusive,
            Set<String> fieldsToIgnore) {
        this.level = level;
        this.count = count;
        this.stop = stop;
        this.jsonStop = jsonStop;
        this.fieldsToIgnore = fieldsToIgnore;
        this.isInclusive = isInclusive;
    }


    public String getJsonStop() {
        return jsonStop;
    }

    public void setJsonStop(String jsonStop) {
        this.jsonStop = jsonStop;
    }

    public boolean isInclusive() {
        return isInclusive;
    }

    public void setInclusive(Boolean inclusive) {
        isInclusive = inclusive;
    }

    public Integer getStop() {
        return stop;
    }

    public void setStop(Integer stop) {
        this.stop = stop;
    }

    public Boolean isStop() {
        return stop.equals(level);
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public void increment() {
        this.level++;
    }

    public Set<String> getFieldsToIgnore() {
        return fieldsToIgnore;
    }

    public void setFieldsToIgnore(Set<String> fieldsToIgnore) {
        this.fieldsToIgnore = fieldsToIgnore;
    }

    @Override
    public String toString() {
        return String.valueOf(level);
    }

}
