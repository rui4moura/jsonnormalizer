package io.gitlab.rui4moura.jsonnormalizer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

class TreeLevelTest {

    private TreeLevel treeLevelUnderTest;

    @BeforeEach
    void setUp() {
        treeLevelUnderTest = new TreeLevel(0, 0L, 0, "", Boolean.FALSE, Collections.emptySet());
    }

    @Test
    void whenIncrements_GivenInitializedAsZero_ThenTreeLevelIsOne() {

        // Run the test
        treeLevelUnderTest.increment();

        // Verify the results
        assertThat(treeLevelUnderTest.getLevel()).isEqualTo(1);
    }

    @Test
    void whenToString_GivenInitializedAsZero_ThenTreeLevelIsStringifiedZero() {

        // Run the test
        final String result = treeLevelUnderTest.toString();

        // Verify the results
        assertThat(result).isEqualTo("0");
    }

    @Test
    void whenInclusiveSetToTrue_GivenInitializedAsFalse_ThenIsInclusive() {

        // Setup
        treeLevelUnderTest.setInclusive(Boolean.TRUE);

        // Run the test and Verify the results
        assertThat(treeLevelUnderTest.isInclusive()).isEqualTo(Boolean.TRUE);
    }

    @Test
    void whenStopAndValueSet_GivenSameValues_ThenIsStop() {

        // Setup
        treeLevelUnderTest.setStop(1);
        treeLevelUnderTest.setLevel(1);

        // Run the test and Verify the results
        assertThat(treeLevelUnderTest.getStop()).isEqualTo(1);
        assertThat(treeLevelUnderTest.getLevel()).isEqualTo(1);
        assertThat(treeLevelUnderTest.isStop()).isEqualTo(Boolean.TRUE);
    }

    @Test
    void whenCount_GivenSomeValue_ThenItsSetted() {

        // Setup
        treeLevelUnderTest.setCount(1L);

        // Run the test and Verify the results
        assertThat(treeLevelUnderTest.getCount()).isEqualTo(1L);
    }

    @Test
    void whenJsonStop_GivenSomeValue_ThenItsSetted() {

        // Setup
        treeLevelUnderTest.setJsonStop("someValue");

        // Run the test and Verify the results
        assertThat(treeLevelUnderTest.getJsonStop()).isEqualTo("someValue");
    }

}
