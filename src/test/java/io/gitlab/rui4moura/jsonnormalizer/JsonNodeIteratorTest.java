package io.gitlab.rui4moura.jsonnormalizer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.json.JsonMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.io.StringReader;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class JsonNodeIteratorTest {

    private static final String JSON_CONTENT = "{\"content\":\"content\"}";
    @Spy
    private JsonMapper mapper;
    @Spy
    private JsonParser parser;
    @Spy
    private JsonNodeIterator jsonNodeIteratorUnderTestMocked;
    private JsonNodeIterator jsonNodeIteratorUnderTest;

    @BeforeEach
    void setUp() throws Exception {
        StringReader reader = new StringReader(JSON_CONTENT);
        MockitoAnnotations.openMocks(this);
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        jsonNodeIteratorUnderTestMocked.setMapper(mapper);
        jsonNodeIteratorUnderTestMocked.setParser(parser);
        jsonNodeIteratorUnderTest = new JsonNodeIterator(reader);
    }

    @Test
    void whenHasNext_GivenInitialized_ThenResultIsTrue() {
        // Run the test
        final boolean result = jsonNodeIteratorUnderTest.hasNext();

        // Verify the results
        assertThat(result).isTrue();
    }

    @Test
    void whenNext_GivenInitialized_ThenResultIsExpected() {

        // Run the test
        final JsonNode result = jsonNodeIteratorUnderTest.next();

        // Verify the results
        assertThat(result).hasToString(JSON_CONTENT);
    }

    @Test
    void whenNext_GivenReadValueAsTreeThrowingIOException_ThenThrowsIllegalStateException() throws IOException {
        // Setup
        when(parser.getCodec()).thenReturn(mapper);
        when(parser.readValueAsTree()).thenThrow(new IOException());


        // Run the test
        assertThatThrownBy(() -> jsonNodeIteratorUnderTestMocked.next()).isInstanceOf(IllegalStateException.class);
    }

    @Test
    void whenSetRoot_GivenNonExistingRoot_ThenReturnsFalse() throws IOException {
        // Setup
        final boolean expectedResult = false;

        // Run the test
        final boolean result = jsonNodeIteratorUnderTest.findJsonRoot("jsonRoot");

        // Verify the results
        assertThat(result).isEqualTo(expectedResult);
    }

    @Test
    void whenSetRoot_GivenGetCurrentNameThrowingIOException_ThenThrowsIOException() throws IOException {
        // Setup
        when(parser.nextToken()).thenReturn(JsonToken.START_OBJECT);
        when(parser.getCurrentName()).thenThrow(new IOException());

        // Run the test
        assertThatThrownBy(() -> jsonNodeIteratorUnderTestMocked.findJsonRoot("jsonRoot"))
                .isInstanceOf(IOException.class);
    }
}
