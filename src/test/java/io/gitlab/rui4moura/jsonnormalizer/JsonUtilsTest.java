package io.gitlab.rui4moura.jsonnormalizer;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.Customization;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.skyscreamer.jsonassert.JSONCompareResult;
import org.skyscreamer.jsonassert.comparator.CustomComparator;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.skyscreamer.jsonassert.comparator.JSONCompareUtil.getKeys;
import static org.skyscreamer.jsonassert.comparator.JSONCompareUtil.qualify;

class JsonUtilsTest {


    private static final String FLATTENED_JSON_FILENAME = "flattened.json";
    private static final String FLATTENED_STOPPED_JSON_FILENAME = "flattened_stopped.json";
    private static final String FLATTENED_STOPPED_INCLUSIVE_JSON_FILENAME = "flattened_stopped_inclusive.json";
    private static final String FLATTENED_SENT_STOPPED_JSON_FILENAME = "flattened_sent_stopped.json";
    private static final String FLATTENED_SENT_STOPPED_INCLUSIVE_JSON_FILENAME = "flattened_sent_stopped_inclusive"
            + ".json";
    private static final String NONFLATTENED_JSON_FILENAME = "nonflat.json";
    private static final String NONFLATTENED_SENT_JSON_FILENAME = "nonflat_sent.json";
    private static final String SRC_TEST_RESOURCES = "src/test/resources/json/";


    @Test
    void whenNormalize_GivenNonFlattenedAndNonExistingRoot_ThenResultIsExpectedFlattenedFile() throws IOException,
            JSONException {
        // Setup
        final File tempFile = File.createTempFile(NONFLATTENED_JSON_FILENAME, ".tmp.json");
        String expected = readJSONToString(SRC_TEST_RESOURCES + FLATTENED_JSON_FILENAME);

        // Run the test
        JsonUtils.normalize(new File(SRC_TEST_RESOURCES, NONFLATTENED_JSON_FILENAME), tempFile, "jsonRoot",
                null, Boolean.FALSE);

        // Verify the results
        String actual = readJSONToString(tempFile.getAbsolutePath());
        JSONAssert.assertEquals(expected, actual, false);
    }

    @Test
    void whenNormalize_GivenNonFlattenedAndExistingRoot_ThenResultIsExpectedFlattenedFile() throws IOException,
            JSONException {
        // Setup
        final File tempFile = File.createTempFile(NONFLATTENED_JSON_FILENAME, ".tmp.json");
        String expected = readJSONToString(SRC_TEST_RESOURCES + FLATTENED_JSON_FILENAME);

        // Run the test
        JsonUtils.normalize(new File(SRC_TEST_RESOURCES, NONFLATTENED_JSON_FILENAME), tempFile, "customers",
                null, Boolean.FALSE);

        // Verify the results
        String actual = readJSONToString(tempFile.getAbsolutePath());
        JSONAssert.assertEquals(expected, actual,
                new AttributeIgnoringComparator(JSONCompareMode.LENIENT,
                        new HashSet<>(Collections.singletonList("concernId")))
        );

    }

    @Test
    void whenNormalize_GivenNonFlattenedWithExistingRootAndExistingStop_ThenResultIsExpectedSemiFlattenedFile()
            throws IOException,
            JSONException {
        // Setup
        final File tempFile = File.createTempFile(NONFLATTENED_JSON_FILENAME, ".tmp.json");
        String expected = readJSONToString(SRC_TEST_RESOURCES + FLATTENED_STOPPED_JSON_FILENAME);

        // Run the test
        JsonUtils.normalize(new File(SRC_TEST_RESOURCES, NONFLATTENED_JSON_FILENAME), tempFile, "concerns",
                "contacts", Boolean.FALSE);

        // Verify the results
        String actual = readJSONToString(tempFile.getAbsolutePath());
        JSONAssert.assertEquals(expected, actual,
                new AttributeIgnoringComparator(JSONCompareMode.LENIENT,
                        new HashSet<>(Collections.singletonList("concernId")))
        );

    }

    @Test
    void whenNormalize_GivenNonFlattenedWithInclusiveExistingRootAndExistingStop_ThenResultIsExpectedSemiFlattenedFile()
            throws IOException,
            JSONException {
        // Setup
        final File tempFile = File.createTempFile(NONFLATTENED_JSON_FILENAME, ".tmp.json");
        String expected = readJSONToString(SRC_TEST_RESOURCES + FLATTENED_STOPPED_INCLUSIVE_JSON_FILENAME);

        // Run the test
        JsonUtils.normalize(new File(SRC_TEST_RESOURCES, NONFLATTENED_JSON_FILENAME), tempFile, "concerns",
                "contacts", Boolean.TRUE);

        // Verify the results
        String actual = readJSONToString(tempFile.getAbsolutePath());
        JSONAssert.assertEquals(expected, actual,
                new AttributeIgnoringComparator(JSONCompareMode.LENIENT,
                        new HashSet<>(Collections.singletonList("concernId")))
        );

    }


    @Test
    void whenNormalize_GivenNonFlattenedSentWithExistingRootAndExistingStop_ThenResultIsExpectedSemiFlattenedFile()
            throws IOException,
            JSONException {
        // Setup
        final File tempFile = File.createTempFile(NONFLATTENED_SENT_JSON_FILENAME, ".tmp.json");
        String expected = readJSONToString(SRC_TEST_RESOURCES + FLATTENED_SENT_STOPPED_JSON_FILENAME);

        // Run the test
        JsonUtils.normalize(new File(SRC_TEST_RESOURCES, NONFLATTENED_SENT_JSON_FILENAME), tempFile, "concerns",
                "monthData", Boolean.FALSE);

        // Verify the results
        String actual = readJSONToString(tempFile.getAbsolutePath());
        JSONAssert.assertEquals(expected, actual,
                new AttributeIgnoringComparator(JSONCompareMode.LENIENT,
                        new HashSet<>(Collections.singletonList("concernId")))
        );

    }


    @Test
    void whenNormalize_GivenNonFlattenedSentWithInclusiveExistingRootAndExistingStop_ThenResultIsExpectedSemiFlattenedFile()
            throws IOException,
            JSONException {
        // Setup
        final File tempFile = File.createTempFile(NONFLATTENED_SENT_JSON_FILENAME, ".tmp.json");
        String expected = readJSONToString(SRC_TEST_RESOURCES + FLATTENED_SENT_STOPPED_INCLUSIVE_JSON_FILENAME);

        // Run the test
        JsonUtils.normalize(new File(SRC_TEST_RESOURCES, NONFLATTENED_SENT_JSON_FILENAME), tempFile, "concerns",
                "monthData", Boolean.TRUE);

        // Verify the results
        String actual = readJSONToString(tempFile.getAbsolutePath());
        JSONAssert.assertEquals(expected, actual,
                new AttributeIgnoringComparator(JSONCompareMode.LENIENT,
                        new HashSet<>(Collections.singletonList("concernId")))
        );

    }

    @Test
    void whenNormalize_GivenIgnoreFieldsFromNonFlattenedSent_ThenResultIsExpectedFileWithoutFields()
            throws IOException,
            JSONException {
        // Setup
        final File tempFile = File.createTempFile(NONFLATTENED_SENT_JSON_FILENAME, ".tmp.json");
        String expected = readJSONToString(SRC_TEST_RESOURCES + FLATTENED_SENT_STOPPED_INCLUSIVE_JSON_FILENAME);

        // Run the test
        JsonUtils.normalize(new File(SRC_TEST_RESOURCES, NONFLATTENED_SENT_JSON_FILENAME), tempFile, "concerns",
                "monthData", Boolean.TRUE, "contactId", "customerId");

        // Verify the results
        String actual = readJSONToString(tempFile.getAbsolutePath());
        JSONAssert.assertEquals(expected, actual,
                new AttributeIgnoringComparator(JSONCompareMode.LENIENT,
                        new HashSet<>(Arrays.asList("concernId", "contactId", "customerId")))
        );

    }

    private String readJSONToString(String filename) throws IOException {
        java.net.URI uri = new File(filename).toURI();
        java.nio.file.Path resPath = java.nio.file.Paths.get(uri);
        return new String(java.nio.file.Files.readAllBytes(resPath), StandardCharsets.UTF_8);
    }

    /**
     * This comparator accepts that the actual does not contain a specified
     * list of specific fields from expected
     */
    static class AttributeIgnoringComparator extends CustomComparator {
        private final Set<String> attributesToIgnore;

        private AttributeIgnoringComparator(JSONCompareMode mode, Set<String> attributesToIgnore,
                Customization... customizations) {
            super(mode, customizations);
            this.attributesToIgnore = attributesToIgnore;
        }

        @Override
        protected void checkJsonObjectKeysExpectedInActual(String prefix, JSONObject expected, JSONObject actual,
                JSONCompareResult result) throws JSONException {
            Set<String> expectedKeys = getKeys(expected);
            expectedKeys.removeAll(attributesToIgnore);
            for (String key : expectedKeys) {
                Object expectedValue = expected.get(key);
                if (actual.has(key)) {
                    Object actualValue = actual.get(key);
                    compareValues(qualify(prefix, key), expectedValue, actualValue, result);
                } else {
                    result.missing(prefix, key);
                }
            }
        }
    }
}
